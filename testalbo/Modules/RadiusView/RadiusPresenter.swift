import Foundation


final class RadiusPresenter: RadiusPresenterProtocol {
    
    var interactor: RadiusInteractorInputProtocol?
    var router: RadiusRouterProtocol?
    weak var view: RadiusViewProtocol?
    
    func openTabView(distance: String) {
        guard let view = view as? RadiusViewController else { return }
        router?.openTabBar(from: view, distance: distance)
    }
    
    
}

extension RadiusPresenter: RadiusInteractorOutputProtocol {
    
}
