import UIKit
import CoreLocation

class RadiusViewController: UIViewController , RadiusViewProtocol {
    var presenter: (RadiusInteractorOutputProtocol & RadiusPresenterProtocol)?
    
    @IBOutlet weak var indicatorLabel: UILabel!
    var currentValue: Int = 1
    private var alertController: UIAlertController? = nil
    @IBOutlet weak var slider: UISlider!
    
    
    private lazy var LocationMgr = {
        return UserLocationManager.SharedManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(checkPermissionStatus), name: UIApplication.didBecomeActiveNotification, object: nil)
        checkPermissionStatus()
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        currentValue = Int(sender.value)
        indicatorLabel.text = "\(currentValue)"
    }
    
    @IBAction func search(_ sender: Any) {
        presenter?.openTabView(distance: "\(Int(slider.value))")
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func checkPermissionStatus() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    print("No access")
                    self.showAlertNoLocationPermission()
                    LocationMgr.delegate = self
                case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                    alertController?.dismiss(animated: true, completion: nil)
                @unknown default:
                break
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func showAlertNoLocationPermission() {
        alertController = UIAlertController(title: "Permiso de ubicación denegado",
            message: "Se requiere tu ubicación para poder mostrar los lugares cercanos ati.\nPor favor habilita el permiso de localización en Privacidad. \n Ve a Privacidad > Servicios de Localización > Yambi > Permitir Siempre",
            preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Configuración", style: .default) { (_) -> Void in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController?.addAction(settingsAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
}

extension RadiusViewController: LocationUpdateDelegate {
    func locationDidUpdateToLocation(location: CLLocation) {
        //
    }
    
}
