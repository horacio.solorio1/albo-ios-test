import Foundation
import UIKit

protocol RadiusInteractorInputProtocol: class {
    
    var presenter: (RadiusPresenterProtocol & RadiusInteractorOutputProtocol)? {get set}
    
    
}

protocol RadiusInteractorOutputProtocol {
    
}

protocol RadiusPresenterProtocol: class {
    func openTabView(distance: String)
}

protocol RadiusViewProtocol: class {
    var presenter: (RadiusPresenterProtocol & RadiusInteractorOutputProtocol)? {get set}
}

protocol RadiusRouterProtocol: class {
    func openTabBar(from view: UIViewController, distance: String)
}
