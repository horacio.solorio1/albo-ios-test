import Foundation
import UIKit

final class RadiusRouter: RadiusRouterProtocol {
    
    func openTabBar(from view: UIViewController, distance: String) {
        let tabBarController = TabBarMain(distance: distance)
        view.navigationController?.pushViewController(tabBarController, animated: true)
    }
    
    
}
