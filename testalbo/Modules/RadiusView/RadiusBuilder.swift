
import Foundation
import UIKit

final class RadiusBuilder {
    static func build() -> UIViewController? {
        let mainNav = UIStoryboard(name: "Main", bundle: .none).instantiateInitialViewController() as? UINavigationController
        
        guard let view: RadiusViewProtocol = mainNav?.topViewController as? RadiusViewController else {
            return nil
        }
        
        let presenter = RadiusPresenter()
        let interactor = RadiusIteractor()
        let router = RadiusRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        view.presenter = presenter
        
        return mainNav
    }
    
}
