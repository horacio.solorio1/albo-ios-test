import Foundation
import Alamofire
import SwiftyJSON

final class MapIteractor: MapInteractorInputProtocol {
    
    weak var presenter: (MapInteractorOutputProtocol & MapPresenterProtocol)?
    
    let apiKey = "1388e0b138mshfa0a56ed1c7cac5p12f415jsn6dc5b6ff550e"

    func viewDidAppear(distance: String, lat: String, lon: String) {
        
        let mainurl = "https://aerodatabox.p.rapidapi.com/airports/search/location/\(lat)/\(lon)/km/\(distance)/16?withFlightInfoOnly=false"
        
        let headers: HTTPHeaders = [
            "x-rapidapi-key": apiKey,
            "x-rapidapi-host": "aerodatabox.p.rapidapi.com"
        ]
        
        AF.request(mainurl,
               method: .get,
               parameters: nil,
               encoding: JSONEncoding.default,
               headers: headers
            )
            .responseJSON {
                response in
                switch response.result {
                case .success (let data):
                    if let value = data as? JSON, let array = value["items"] as? [JSON] {
                        let airports = array.map({Airport.init(json: $0)}).compactMap {$0}
                        print(airports)
                        if (!airports.isEmpty) {
                            var airportMarkers: [AirportMarkerAnnotation] = []
                            for airport in airports {
                                if airport.name != nil && airport.latitude != 0 && airport.longitude != 0 {
                                    let airportMarker = AirportMarkerAnnotation(airport.latitude, airport.longitude, name: airport.name ?? "")
                                    airportMarkers.append(airportMarker)
                                }
                            }
                            self.presenter?.nearbyAirports(airports: airportMarkers)
                        } else {
                            self.presenter?.showEmptyResults()
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
    }
    
}
