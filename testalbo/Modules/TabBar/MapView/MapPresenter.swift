import Foundation


final class MapPresenter: MapPresenterProtocol {
    
    var interactor: MapInteractorInputProtocol?
    var router: MapRouterProtocol?
    weak var view: MapViewProtocol?
    
    var distance = "1"
    
    func viewDidAppear(lat: String, lon: String) {
        interactor?.viewDidAppear(distance: distance, lat: lat, lon: lon)
    }
    
}


extension MapPresenter: MapInteractorOutputProtocol {
    func nearbyAirports(airports: [AirportMarkerAnnotation]) {
        view?.showMarkers(markers: airports)
    }
    
    func showEmptyResults() {
        view?.showEmptyResults()
    }
    
    
}
