import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController , MapViewProtocol, LocationUpdateDelegate, MKMapViewDelegate {
    
    var presenter: (MapInteractorOutputProtocol & MapPresenterProtocol)?
    
    private let LocationMgr = UserLocationManager.SharedManager
    
    @IBOutlet weak var mapView: MKMapView?
    private var markers: [AirportMarkerAnnotation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        LocationMgr.delegate = self
        setUpMapView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let lastLocation = LocationMgr.lastLocation {
            presenter?.viewDidAppear(lat: "\(lastLocation.coordinate.latitude)", lon: "\(lastLocation.coordinate.longitude)")
        }
    }
    
    func locationDidUpdateToLocation(location: CLLocation) {
        centerMapOnLocation(location: location)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let dortmunRegion = MKCoordinateRegion(
            center: CLLocationCoordinate2D(
                latitude: location.coordinate.latitude,
                longitude: location.coordinate.longitude),
            span: MKCoordinateSpan(
                latitudeDelta: 0.5,
                longitudeDelta: 0.5))
        mapView?.setRegion(dortmunRegion, animated: true)
        
    }
    
    //MARK: - Setup Methods
    func setUpMapView() {
        // mapView?.delegate = self
        mapView?.showsUserLocation = true
        mapView?.showsCompass = true
        mapView?.showsScale = true
        if let lastLocation = LocationMgr.lastLocation {
            centerMapOnLocation(location: lastLocation)
        }
        
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func showMarkers(markers: [AirportMarkerAnnotation]) {
        removeMarkers()
        mapView?.addAnnotations(markers)
        self.markers = markers
    }
    
    func removeMarkers() {
        if let allAnnotations = mapView?.annotations {
            mapView?.removeAnnotations(allAnnotations)
        }
    }
    func showEmptyResults() {
        let alert = UIAlertController(title: "Aviso", message: "No hay resultados intenta con un radio mas amplio", preferredStyle: .alert)
        let done = UIAlertAction(title: "Entendido", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(done)
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
