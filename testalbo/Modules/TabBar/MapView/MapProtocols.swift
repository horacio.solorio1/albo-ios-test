import Foundation


protocol MapInteractorInputProtocol: class {
    
    var presenter: (MapPresenterProtocol & MapInteractorOutputProtocol)? {get set}
    
    func viewDidAppear(distance: String, lat: String, lon: String)
}

protocol MapInteractorOutputProtocol {
    func nearbyAirports(airports: [AirportMarkerAnnotation])
    func showEmptyResults()
}

protocol MapPresenterProtocol: class {
    func viewDidAppear(lat: String, lon: String)
}

protocol MapViewProtocol: class {
    var presenter: (MapPresenterProtocol & MapInteractorOutputProtocol)? {get set}
    
    func showMarkers(markers: [AirportMarkerAnnotation])
    func showEmptyResults()
}

protocol MapRouterProtocol: class {
    
}
