
import Foundation
import UIKit

final class MapBuilder {
    static func build(distance: String) -> UIViewController? {
       
        let mapnav = UIStoryboard(name: "Main", bundle: .none).instantiateViewController(withIdentifier: "MapInit") as? UINavigationController
        
        guard let view: MapViewProtocol = mapnav?.topViewController as? MapViewController else {
            return nil
        }
        
        let presenter = MapPresenter()
        presenter.distance = distance
        let interactor = MapIteractor()
        let router = MapRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        view.presenter = presenter
        
        
        return mapnav
    }
    
}
