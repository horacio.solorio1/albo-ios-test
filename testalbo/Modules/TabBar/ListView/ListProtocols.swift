import Foundation


protocol ListInteractorInputProtocol: class {
    
    var presenter: (ListPresenterProtocol & ListInteractorOutputProtocol)? {get set}
    
    
}

protocol ListInteractorOutputProtocol {
    
}

protocol ListPresenterProtocol: class {

}

protocol ListViewProtocol: class {
    var presenter: (ListPresenterProtocol & ListInteractorOutputProtocol)? {get set}
}

protocol ListRouterProtocol: class {
    
}
