import UIKit

class ListViewController: UIViewController , ListViewProtocol {
    var presenter: (ListInteractorOutputProtocol & ListPresenterProtocol)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
}
