
import Foundation
import UIKit

final class ListBuilder {
    static func build() -> UIViewController? {
        let listnav = UIStoryboard(name: "Main", bundle: .none).instantiateViewController(withIdentifier: "ListInit") as? UINavigationController
        
        guard let view: ListViewProtocol = listnav?.topViewController as? ListViewController else {
            return nil
        }
        
        let presenter = ListPresenter()
        let interactor = ListIteractor()
        let router = ListRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        view.presenter = presenter
        
        return listnav
    }
    
}
