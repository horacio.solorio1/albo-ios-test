import Foundation


final class ListPresenter: ListPresenterProtocol {
    
    var interactor: ListInteractorInputProtocol?
    var router: ListRouterProtocol?
    weak var view: ListViewProtocol?
    
    
}


extension ListPresenter: ListInteractorOutputProtocol {
    
}
