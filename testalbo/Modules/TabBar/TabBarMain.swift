import UIKit

class TabBarMain: UITabBarController {
    
    var distance = "1"
    
    init(distance: String) {
        self.distance = distance
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let map = MapBuilder.build(distance: distance),
            let list = ListBuilder.build() else { return }
        viewControllers = [map, list]
        
    }

}
