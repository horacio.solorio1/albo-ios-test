//
//  AppDelegate.swift
//  testalbo
//
//  Created by Tato on 26/01/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let mainController = RadiusBuilder.build()
        window?.rootViewController = mainController
        return true
    }


}

