//
//  LocationService.swift
//  testalbo
//
//  Created by Tato on 26/01/21.
//

import Foundation
import CoreLocation
import MapKit
import UIKit

protocol LocationUpdateDelegate {
  func locationDidUpdateToLocation(location : CLLocation)
}

/// Notification on update of location. UserInfo contains CLLocation for key "location"
let kLocationDidChangeNotification = "LocationDidChangeNotification"

class UserLocationManager: NSObject, CLLocationManagerDelegate {
    
    static let SharedManager = UserLocationManager()
    private var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var delegate: LocationUpdateDelegate?
    var lastLocation: CLLocation? = nil

    private override init () {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLLocationAccuracyBestForNavigation
        handleAutorizationStatus(locationManager: locationManager, status: CLLocationManager.authorizationStatus())
        self.locationManager.startUpdatingLocation()
    }
    
    func handleAutorizationStatus(locationManager :CLLocationManager, status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
        case .restricted, .denied:
            print("Solocitar los permisos")
            locationManager.requestAlwaysAuthorization()
        case .authorizedWhenInUse:
            locationManager.requestAlwaysAuthorization()
        case .authorizedAlways:
            self.locationManager.startUpdatingLocation()
        @unknown default:
            print("No se puede obtener datos de localización")
        }
    }

    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.first {
            let userInfo : NSDictionary = ["location" : currentLocation]
            self.delegate?.locationDidUpdateToLocation(location: currentLocation)
            NotificationCenter
                .default
                .post(name: NSNotification.Name(rawValue: kLocationDidChangeNotification),
                      object: self,
                      userInfo: userInfo as [NSObject : AnyObject])
            lastLocation = currentLocation
        }
        
    }
        
    func getLastLocation() -> CLLocation? {
        return lastLocation
    }
}

