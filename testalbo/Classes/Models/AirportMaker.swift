//
//  AirportMaker.swift
//  testalbo
//
//  Created by Tato on 27/01/21.
//

import UIKit
import MapKit

class AirportMarkerAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var name: String
    
    init(_ latitude:CLLocationDegrees,_ longitude: CLLocationDegrees, name: String){
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        self.name = name
    }
    
}
