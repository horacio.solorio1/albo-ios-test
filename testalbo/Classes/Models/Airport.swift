//
//  Airport.swift
//  testalbo
//
//  Created by Tato on 27/01/21.
//

import Foundation
import Unbox

final class Airport: Decodable {
    let name: String?
    var latitude: Double = 0
    var longitude: Double = 0
    
    
    init(unboxer: Unboxer) throws {
        name = try unboxer.unbox(key: "name")
        latitude = try unboxer.unbox(keyPath: "location.lat")
        longitude = try unboxer.unbox(keyPath: "location.lon")
    }
}
