//
//  Decodeable.swift
//  testalbo
//
//  Created by Tato on 27/01/21.
//

import Foundation
import Unbox

typealias JSON = [String: Any]

protocol Decodable: Unboxable {
    init?(json: JSON)
}

extension Decodable {
    init?(json: JSON) {
        try? self.init(unboxer: Unboxer(dictionary: json))
    }
}
